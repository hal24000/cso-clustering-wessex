import pickle 
from _plotly_future_ import v4_subplots
import plotly.graph_objects as go
from plotly.subplots import make_subplots

def load_obj(name ):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)
    
# opens dic of noe to cluster from pickele dict 
dic_node_to_cluster = load_obj( 'node_to_12cluster_dict-13-7-21-run1' )

def plot_clusters_of_pair(df, nodes, date_col = 'Datetime',):
    """Returns node trace and plots on cluster from DTW  kmeans 
    
    df - dataframe storing clusters and node values across the timseries - 
    nodes - list of 2 nodes to compare 
    
    """
    
    cluster_cols = [f"cluster_{dic_node_to_cluster[nodes[0]]+1}", f"cluster_{dic_node_to_cluster[nodes[1]]+1}"]
    
    if cluster_cols[0] != cluster_cols[1]:
        print(f"Nodes {nodes[0]} and {nodes[1]} are in clusters: {cluster_cols[0]} and {cluster_cols[1]} respectively ")
        fig = make_subplots(rows=1, cols=2)
    
        fig.add_trace(go.Scatter(x=df[date_col], 
                                    y=df[cluster_cols[0]], 
                                 mode="lines", 
                                name = cluster_cols[0] ), 
                      row=1, col=1)


        fig.add_trace(go.Scatter(x=df[date_col], 
                                    y=df[nodes[0]], 
                                 mode="lines", 
                                 line_color= 'red', 
                                name = nodes[0]), 
                      row=1, col=1)



        fig.add_trace(go.Scatter(x=df[date_col], 
                                    y=df[cluster_cols[1]], 
                                 name = cluster_cols[1], 
                                 mode="lines"), 
                      row=1, col=2)


        fig.add_trace(go.Scatter(x=df[date_col], 
                                    y=df[nodes[1]], 
                                 mode="lines", 
                                 line_color= 'blue', 
                                name = nodes[1]), 
                      row=1, col=2)
    else: 
        print(f"Nodes {nodes[0]} and {nodes[1]} are both in cluster: {cluster_cols[0]}")
        fig = make_subplots(rows=1, cols=1)
    
        fig.add_trace(go.Scatter(x=df[date_col], 
                                    y=df[cluster_cols[0]], 
                                 mode="lines", 
                                name = cluster_cols[0] ), 
                      row=1, col=1)


        fig.add_trace(go.Scatter(x=df[date_col], 
                                    y=df[nodes[0]], 
                                 mode="lines", 
                                 line_color= 'red', 
                                name = nodes[0]), 
                      row=1, col=1)


        fig.add_trace(go.Scatter(x=df[date_col], 
                                    y=df[nodes[1]], 
                                 mode="lines", 
                                 line_color= 'blue', 
                                name = nodes[1]), 
                      row=1, col=1)


    
    
    fig.update_xaxes(rangeslider_visible=True)
    
    return fig
  
    
    