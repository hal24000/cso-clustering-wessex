import pickle
import urllib.parse
import pandas as pd
from pymongo import MongoClient

from setup.database import db

###### Dataframes from database 
def get_df_of_collection(collection, db = db, query = {}, subset = {'_id' : 0}):
    """For a collection get a dataframe"""
    df = pd.DataFrame(list((db[collection].find( query, subset)))) 
    return df 

def get_ordered_df_from_collection(collection, sort_col = 'Datetime'):
    """Gets a collection as an ordered collection"""
    df = get_df_of_collection(collection =  collection)
    df = df.sort_values(sort_col)
    df.reset_index(inplace = True)
    df = df.drop('index', axis =1 ) # might fail here 
    return df

def open_pickle_model(filepath):
    """Opens a pickle file from local file path"""
    with open(filepath, 'rb') as f:
        file = pickle.load(f)
        return file 

def get_clusters_df(pickle_obj):
    """from pickle model get the dataFrame of clusters"""
    # get data and reshape 
    data = pickle_obj['model_params']['cluster_centers_'].reshape( pickle_obj['model_params']['cluster_centers_'].shape[0], 
                                                 pickle_obj['model_params']['cluster_centers_'].shape [1], 
                                                                 )
    #make dataframe 
    df_clusters = pd.DataFrame(data = data).T
    
    # rename columns 
    colrename = [f"cluster_{i+1}" for i in list(df_clusters)]
    renamedict = {k : v for k, v in zip( list( df_clusters), colrename )}
    df_clusters.rename(renamedict, axis =1, inplace = True)
    
    return df_clusters

def get_cluster_for_nodes(df_nodes, pickle_model):
    """Gets the cluster for each node in pciklefile dependiong on orginal dataframe """
    nodes_all = [i for i in list(df_nodes) if i not in ['Datetime', 'Precipitation', 'rain_shift-1',  'rain_6h_mean',  'rain_6h_sum'] ]
    dic_node_to_cluster = { k: v for k, v in zip(nodes_all, pickle_model['model_params']['labels_'])}
    return dic_node_to_cluster
