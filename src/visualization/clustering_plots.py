from _plotly_future_ import v4_subplots
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots


class ClusteringPlots():
    
    """Class for all plots related to CSO clustering app"""
    
    def plot_all_clusters(self, dfclusters, CLUSTERS):
        """For clusters plot each cluster"""
        if CLUSTERS % 4 == 0:
            cols_ = 4
        elif CLUSTERS % 3 == 0:
            cols_ = 3 
        else:
            print("nah")
        rows_ = int(CLUSTERS/cols_)
        #rows_ = math.ceil(CLUSTERS/cols_)

        fig = make_subplots(
            rows= rows_  , cols= cols_,
            column_widths=[0.25]*cols_, 

            specs= [[{"rowspan": 1, "colspan": 1} for i in range(0,cols_)]]*rows_, 
            #print_grid=True
            )

        cluster = 1
        col = 0
        for r in range(1, rows_+1):
            #print(r)
            for col_ in range(1, cols_+1):
                if col_ <= cols_:
                    #print("row", r)
                    #print("col", col_)
                    fig.add_trace(go.Scatter(x=dfclusters['Datetime'], 
                                 y=dfclusters[f'cluster_{cluster}'], 
                                 name=f'cluster_{cluster}',
                                 line=dict(color='firebrick', 
                                           width=1)),row=r, col=col_)
                    col += 1
                    cluster +=1
                #while col_ > cols_:
                    #col
        fig.update_xaxes(showticklabels=False) # hide all the xticks
        
        return fig
    
    def plot_sensor_clusters_by_loc(self, dfloc, cluster_number, selected_node_1, selected_node_2):
        """Plots locations of sensors colored by cluster"""

        print("selected nodes")
        print(selected_node_1)
        print(selected_node_2)

        fig = px.scatter_mapbox(dfloc, 
                             lat= dfloc['latitude'],
                             lon= dfloc['longitude'],
                             color = 'cluster', 
                             text = 'clusters_text', 
                             zoom=11,
                            labels={
                             f'clusters_{cluster_number}': "Clusters/Sensor"
                            },
                             )


        fig.add_trace(go.Scattermapbox(  
                             lat= dfloc.loc[dfloc["node_id"] == selected_node_1 ]['latitude'].values, 
                             lon= dfloc.loc[dfloc["node_id"] == selected_node_1 ]['longitude'].values,
                            marker=dict(size=14, color = 'blue'), 
                            name = 'Sensor 1',
                            text="country"

                             )
                     )

        fig.add_trace(go.Scattermapbox(  
                             lat= dfloc.loc[dfloc["node_id"] == selected_node_2 ]['latitude'].values, 
                             lon= dfloc.loc[dfloc["node_id"] == selected_node_2 ]['longitude'].values,
                            marker=dict(size=14, color = 'red'), 
                            name = 'Sensor 2', 
                             )
                     )



        fig.update_layout(mapbox_style="carto-positron",)
        fig.update_geos(fitbounds="locations")

        clusters = [j for j in range(0, cluster_number) ]
        sensors = [-2, -1]
        sensors.extend(clusters)
        print("sensors", sensors)
        sensors = [fig.data[i] for i in sensors]
        fig.data = tuple(sensors)

        return fig
    
    def plot_clusters_of_pair(self, df, nodes,  dic_node_to_cluster, date_col = 'Datetime',):
        """Returns node trace and plots on cluster from DTW  kmeans 

        df - dataframe storing clusters and node values across the timseries - 
        nodes - list of 2 nodes to compare 

        """

        cluster_cols = [f"cluster_{dic_node_to_cluster[nodes[0]]}", f"cluster_{dic_node_to_cluster[nodes[1]]}"] #lists cluster 


        #cluster_cols =  nodes #[f"cluster_{dic_node_to_cluster[nodes[0]], f"cluster_{dic_node_to_cluster[nodes[1]]}"]


        if cluster_cols[0] != cluster_cols[1]:
            print(f"Nodes {nodes[0]} and {nodes[1]} are in clusters: {cluster_cols[0]} and {cluster_cols[1]} respectively ")
            fig = make_subplots(rows=1, cols=2)

            fig.add_trace(go.Scatter(x=df[date_col], 
                                        y=df[cluster_cols[0]], 
                                     mode="lines", 
                                    name = cluster_cols[0] ), 
                          row=1, col=1)


            fig.add_trace(go.Scatter(x=df[date_col], 
                                        y=df[nodes[0]], 
                                     mode="lines", 
                                     line_color= 'red', 
                                    name = nodes[0]), 
                          row=1, col=1)



            fig.add_trace(go.Scatter(x=df[date_col], 
                                        y=df[cluster_cols[1]], 
                                     name = cluster_cols[1], 
                                     mode="lines"), 
                          row=1, col=2)


            fig.add_trace(go.Scatter(x=df[date_col], 
                                        y=df[nodes[1]], 
                                     mode="lines", 
                                     line_color= 'blue', 
                                    name = nodes[1]), 
                          row=1, col=2)
        else: 
            print(f"Nodes {nodes[0]} and {nodes[1]} are both in cluster: {cluster_cols[0]}")
            fig = make_subplots(rows=1, cols=1)

            fig.add_trace(go.Scatter(x=df[date_col], 
                                        y=df[cluster_cols[0]], 
                                     mode="lines", 
                                    name = cluster_cols[0] ), 
                          row=1, col=1)


            fig.add_trace(go.Scatter(x=df[date_col], 
                                        y=df[nodes[0]], 
                                     mode="lines", 
                                     line_color= 'red', 
                                    name = nodes[0]), 
                          row=1, col=1)


            fig.add_trace(go.Scatter(x=df[date_col], 
                                        y=df[nodes[1]], 
                                     mode="lines", 
                                     line_color= 'blue', 
                                    name = nodes[1]), 
                          row=1, col=1)




        fig.update_xaxes(rangeslider_visible=True)

        return fig
    
    def plot_cost_matrix(self, s1, s2, best_path, df, node1, node2):
        fig = make_subplots(
        rows=4, cols=2,
        column_widths=[0.3, 0.7], 

        specs=[
               [None, {"rowspan": 1, "colspan": 1}], 
               [{"rowspan": 3, "colspan": 1}, {"rowspan": 3, "colspan": 1}], 
               [None, None],
               [None, None],
               #[None, None],
               #[{}, {}]
        ],
        #print_grid=True
        )

        fig.add_trace(go.Scatter(x= df['Datetime'][-24*7: ], #[i for i in range (0, len(s1))], 
                         y=s1.reshape(-24*7, ),  
                                 name= node1), 
                              row=1, col=2)

        x = [i[0] for i in best_path ]
        y = [i[1] for i in best_path ]
        fig.add_trace(go.Scatter(x = x,  #.reverse(), 
                                 y= y,  #.reverse(), 
                                 #name="cost matrix", 
                                 #xaxis = dict(autorange = "reversed")
                                ), 
                                  row=2, col=2)

        fig.add_trace(go.Scatter(x=  s2.reshape(-24*7, ), #[i for i in range (0, len(s1))], 
                                 y=df['Datetime'][-24*7: ], 
                                 name= node2, 
                                ), 
                              row=2, col=1)


        fig.update_layout(height = 1000, width = 1000, 

                         )

        fig.update_xaxes(side="top", showgrid=False, row=1, col=2, 

                        )

        fig.update_yaxes( showgrid=False, row=2, col=2, 
                         autorange='reversed', 
                        )
        #fig.update_xaxes(title_text="cost matrix", showgrid=False, row=3, col=1, 
        #                 autorange='reversed', 
        #                )
        fig.update_xaxes( showgrid=False, row=2, col=1, 
                         autorange='reversed', 
                        )
        fig.update_yaxes( showgrid=False, row=2, col=1, 
                         autorange='reversed', 
                        )

        fig.update_layout(height=1000, width=1000, 
                          title_text="Cost Matrix - last Four Weeks"
                         )
        return fig
